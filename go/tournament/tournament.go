package tournament

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
)

/*
   MP: Matches Played
   W: Matches Won
   D: Matches Drawn (Tied)
   L: Matches Lost
   P: Points
*/

type Team struct {
	Name string
	MP   int
	W    int
	D    int
	L    int
	P    int
}

func Tally(R io.Reader, W io.Writer) error {

	scanner := bufio.NewScanner(R)
	tournament := make(map[string]*Team)

	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}

		if line[0] == '#' {
			continue
		}

		event := strings.Split(line, ";")
		if len(event) != 3 {
			return errors.New("Bad input format")
		}

		homeTeam := event[0]
		visitTeam := event[1]

		if tournament[homeTeam] == nil {
			tournament[homeTeam] = new(Team)
			tournament[homeTeam].Name = homeTeam
		}
		if tournament[visitTeam] == nil {
			tournament[visitTeam] = new(Team)
			tournament[visitTeam].Name = visitTeam
		}
		tournament[homeTeam].MP++
		tournament[visitTeam].MP++
		switch event[2] {
		case "win":
			tournament[homeTeam].W++
			tournament[homeTeam].P = tournament[homeTeam].P + 3
			tournament[visitTeam].L++
		case "draw":
			tournament[homeTeam].D++
			tournament[homeTeam].P++
			tournament[visitTeam].D++
			tournament[visitTeam].P++
		case "loss":
			tournament[visitTeam].W++
			tournament[visitTeam].P = tournament[visitTeam].P + 3
			tournament[homeTeam].L++
		default:
			return errors.New("Not valid outcome")
		}
	}

	positions := []*Team{}
	for _, value := range tournament {
		positions = append(positions, value)
	}

	sort.Slice(positions, func(i, j int) bool {
		return positions[i].Name < positions[j].Name
	})

	sort.Slice(positions, func(i, j int) bool {
		return positions[i].MP > positions[j].MP
	})

	sort.Slice(positions, func(i, j int) bool {
		return positions[i].P > positions[j].P
	})

	fmt.Fprintf(W, "%-30s | MP |  W |  D |  L |  P\n", "Team")

	for _, team := range positions {
		fmt.Fprintf(
			W,
			"%-30s |  %d |  %d |  %d |  %d |  %d\n",
			team.Name,
			team.MP,
			team.W,
			team.D,
			team.L,
			team.P,
		)
	}

	return nil
}
