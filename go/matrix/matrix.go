package matrix

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Matrix struct {
	rows [][]int
	cols [][]int
}

func (m *Matrix) Rows() [][]int {
	return m.rows
}

func (m *Matrix) Cols() [][]int {
	return m.cols
}

func (m *Matrix) Set(row int, col int, val int) bool {

	m.rows[row][col] = val
	m.cols[col][row] = val
	return true
}

func New(input string) (*Matrix, error) {

	rowsString := strings.Split(input, "\n")
	rowCount := len(rowsString)
	for idx := range rowsString {
		if len(rowsString[idx]) != rowCount {
			return nil, errors.New("Uneven rows")
		}
	}
	colCount := len(strings.Fields(rowsString[0]))

	fmt.Printf("rowCount %d\n", rowCount)
	fmt.Printf("colCount %d\n", colCount)
	fmt.Printf("Fields %q\n", strings.Fields(input))

	rows := make([][]int, rowCount)
	for i := range rows {
		rows[i] = make([]int, colCount)
	}
	cols := make([][]int, colCount)
	for i := range cols {
		cols[i] = make([]int, rowCount)
	}

	fmt.Println()
	if !strings.Contains(input, "\n") {
		return nil, errors.New("No newlines \\n")
	}

	rowCounter := 0
	colCounter := 0

	for _, number := range strings.Fields(input) {

		if colCounter == rowCount {
			rowCounter++
			colCounter = 0
		}

		var num int
		fmt.Printf("Number %q\n", number)

		num, err := strconv.Atoi(number)
		if err != nil {
			return nil, errors.New("Si")
		}

		rows[rowCounter][colCounter] = num
		cols[colCounter][rowCounter] = num
		colCounter++
	}

	fmt.Println(rows, cols)

	return &Matrix{rows, cols}, nil
}
