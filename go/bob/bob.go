package bob

import s "strings"

// Hey this does
func Hey(remark string) string {

	remark = s.TrimRight(remark, "\n\t ")

	if !s.HasSuffix(remark, "?") && s.ToUpper(remark) == remark && s.ToLower(remark) != remark {
		return "Whoa, chill out!"
	}

	if s.HasSuffix(remark, "?") && s.ToUpper(remark) == remark && s.ToLower(remark) != remark {
		return "Calm down, I know what I'm doing!"
	}

	if s.HasSuffix(remark, "?") {
		return "Sure."
	}

	if s.Trim(remark, "\n\t\r ") == "" {
		return "Fine. Be that way!"
	}

	return "Whatever."
}
