package robotname

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type Robot struct {
	name string
}

func (r *Robot) Name() (string, error) {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	fmt.Printf("")
	var name string
	if r.name == "" {
		for quant := 0; quant <= 1; {
			num := r1.Intn(90)
			if num > 64 {
				name += string(rune(num))
				quant++
			}
		}
		r.name = name + strconv.Itoa(r1.Intn(999))
	}
	return r.name, nil
}

func (r *Robot) Reset() {
	r.name = ""
}
