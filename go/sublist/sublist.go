package sublist

import (
	"reflect"
)

// Relation a string type that shows the relation between two list
type Relation string

// IntSlice a
type IntSlice []int

func isSublist(smallerList []int, biggerList []int) bool {
	var isSublist bool
	slideMovesMax := len(biggerList) - len(smallerList)
	for idx := 0; idx <= slideMovesMax; idx++ {

		subSlice := biggerList[idx : idx+len(smallerList)]

		isSublist = true
		for idx, value := range subSlice {
			if value != smallerList[idx] {
				isSublist = false
			}
		}

		if isSublist {
			break
		}

	}
	return isSublist
}

// Sublist comment
func Sublist(listOne []int, listTwo []int) Relation {

	var result string
	lengthOfOne := len(listOne)
	lengthOfTwo := len(listTwo)

	if lengthOfOne < lengthOfTwo {
		result = "sublist"
		if lengthOfOne != 0 && !isSublist(listOne, listTwo) {

			result = "unequal"
		}
	} else if lengthOfOne > lengthOfTwo {
		result = "superlist"
		if lengthOfTwo != 0 && !isSublist(listTwo, listOne) {
			result = "unequal"
		}
	} else {
		result = "unequal"
		if reflect.DeepEqual(listOne, listTwo) {
			result = "equal"
		}
	}

	return Relation(result)
}
