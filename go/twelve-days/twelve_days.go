package twelve

import (
	"bytes"
	"fmt"
)

func Song() string {
	var song bytes.Buffer
	for x := 1; x <= 12; x++ {
		song.WriteString(Verse(x))
		if x != 12 {
			song.WriteString("\n")
		}
	}
	return song.String()
}

type Item struct {
	count string
	item  string
}

func Verse(verseCount int) string {
	fmt.Printf("")
	Items := []*Item{
		&Item{count: "first", item: "a Partridge in a Pear Tree."},
		&Item{count: "second", item: "two Turtle Doves, and "},
		&Item{count: "third", item: "three French Hens, "},
		&Item{count: "fourth", item: "four Calling Birds, "},
		&Item{count: "fifth", item: "five Gold Rings, "},
		&Item{count: "sixth", item: "six Geese-a-Laying, "},
		&Item{count: "seventh", item: "seven Swans-a-Swimming, "},
		&Item{count: "eighth", item: "eight Maids-a-Milking, "},
		&Item{count: "ninth", item: "nine Ladies Dancing, "},
		&Item{count: "tenth", item: "ten Lords-a-Leaping, "},
		&Item{count: "eleventh", item: "eleven Pipers Piping, "},
		&Item{count: "twelfth", item: "twelve Drummers Drumming, "},
	}

	var buffer bytes.Buffer
	part1 := "On the %s day of Christmas my true love gave to me: "
	fmt.Fprintf(&buffer, part1, Items[verseCount-1].count)
	for idx := verseCount - 1; idx >= 0; idx-- {
		fmt.Fprintf(&buffer, "%s", Items[idx].item)
	}

	return buffer.String()
}
