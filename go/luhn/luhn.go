package luhn

import (
	S "strings"
	U "unicode"
)

// Valid valid luhn
func Valid(input string) bool {
	input = S.Replace(input, " ", "", -1)
	if len(input) < 2 {
		return false
	}

	numbers := make([]int, len(input))
	for idx, runeV := range input {
		if !U.IsNumber(runeV) {
			return false
		}
		numbers[idx] = int(runeV - '0')
	}

	flipflop := false
	for idx := len(numbers) - 1; idx >= 0; idx-- {
		if flipflop {
			numbers[idx] *= 2
			if numbers[idx] > 9 {
				numbers[idx] = numbers[idx] - 9
			}
		}
		flipflop = !flipflop
	}
	sum := 0
	for _, num := range numbers {
		sum += num
	}
	return sum%10 == 0
}
