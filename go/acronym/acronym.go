package acronym

import (
	"strings"
	"unicode"
)

// Abbreviate should have a comment documenting it.
func Abbreviate(STRING string) string {

	f := func(c rune) bool {
		if string(c) == "'" {
			return false
		}
		return !unicode.IsLetter(c)
	}

	var acronym string
	for _, w := range strings.FieldsFunc(STRING, f) {
		acronym += strings.ToUpper(string(w[0]))
	}

	return acronym
}
