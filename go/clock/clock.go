package clock

import (
	"fmt"
	"math"
	SC "strconv"
)

// Clock clock structure
type Clock struct {
	h int
	m int
}

// String comment
func (c Clock) String() string {

	fmt.Printf("")
	hour := SC.Itoa(c.h)
	min := SC.Itoa(c.m)

	if len(min) < 2 {
		min = "0" + min
	}
	if len(hour) < 2 {
		hour = "0" + hour
	}
	return hour + ":" + min
}

// Add comment
func (c Clock) Add(minutes int) Clock {
	return c
}

// Subtract comment
func (c Clock) Subtract(minutes int) Clock {
	return c
}

// MinutesOver comment
func (c Clock) MinutesOver() Clock {
	whole, dec := math.Modf(float64(c.m) / 60)
	c.m = int(math.Ceil(dec * 60))
	c.h = c.h + int(whole)
	return c
}

// MinutesNegative comment
func (c Clock) MinutesNegative() Clock {
	whole, dec := math.Modf(float64(c.m) / 60)
	c.m = int(dec*60) + 60
	fmt.Println("whole", whole)
	c.h = c.h + int(whole) - 1
	return c
}

// HoursOver comment
func (c Clock) HoursOver() Clock {
	_, dec := math.Modf(float64(c.h) / 24)
	c.h = int(dec * 24)
	return c
}

// HoursUnder comment
func (c Clock) HoursUnder() Clock {
	fmt.Println("hours ", c.h)
	mod := c.h % 24
	fmt.Println("mod ", mod)
	c.h = 24 + mod
	return c
}

// Check comment
func (c Clock) Check() Clock {

	if c.m >= 60 {
		c = c.MinutesOver()
	}

	if c.m < 0 {
		c = c.MinutesNegative()
	}

	if c.h >= 24 {
		c = c.HoursOver()
	}

	if c.h < 0 {
		c = c.HoursUnder()
	}

	return c
}

// New comment
func New(h int, m int) Clock {
	c := Clock{h, m}.Check()
	return c
}
