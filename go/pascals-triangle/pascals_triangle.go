package pascal

import (
	"fmt"
)

func generateRow(row int) []int {
	if row == 0 {
		return []int{0: 1}
	}
	previousRow := generateRow(row - 1)
	currentRow := make([]int, row+1)
	for i := 0; i <= row; i++ {
		var rightSideValue int
		var leftSideValue int
		if i != 0 && i != len(previousRow) {
			leftSideValue = previousRow[i-1]
			rightSideValue = previousRow[i]
		}
		if i == 0 {
			rightSideValue = previousRow[i]
			leftSideValue = 0
		}
		if i == len(previousRow) {
			leftSideValue = previousRow[len(previousRow)-1]
			rightSideValue = 0
		}
		currentRow[i] = leftSideValue + rightSideValue
	}
	return currentRow
}

// Triangle x
func Triangle(size int) [][]int {

	triangle := make([][]int, size)
	for i := range triangle {
		triangle[i] = generateRow(i)
	}

	fmt.Printf("%+v\n", triangle)
	return triangle
}
