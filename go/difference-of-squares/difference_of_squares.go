package diffsquares

// SquareOfSum comment
func SquareOfSum(n int) int {
	var sum int
	for idx := 0; idx <= n; idx++ {
		sum += idx
	}
	return sum * sum
}

// SumOfSquares comment
func SumOfSquares(n int) (sum int) {
	for idx := 0; idx <= n; idx++ {
		sum += idx * idx
	}
	return
}

// Difference comment
func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
