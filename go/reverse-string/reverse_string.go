package reverse

import (
	S "strings"
)

// Reverse reverses word
func Reverse(word string) string {
	var reverse S.Builder

	runes := []rune(word)
	for idx := len(runes) - 1; idx >= 0; idx-- {
		reverse.WriteRune(runes[idx])
	}
	return reverse.String()
}
