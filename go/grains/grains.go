package grains

import (
	"errors"
)

// Square comment
func Square(input int) (uint64, error) {
	if input < 1 || input > 64 {
		return 0, errors.New("")
	}
	return 1 << uint(input-1), nil
}

// Total comment
func Total() (sum uint64) {
	for i := 1; i < 65; i++ {
		value, _ := Square(i)
		sum += value
	}
	return
}
