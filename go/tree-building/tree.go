package tree

import (
	E "errors"
	"fmt"
	"sort"
)

// Record c
type Record struct {
	ID     int
	Parent int
}

// Node c
type Node struct {
	ID       int
	Children []*Node
}

func appendToNode(id int, parentToChildren map[int][]int) *Node {

	if len(parentToChildren[id]) == 0 {
		return &Node{ID: id}
	}

	childList := parentToChildren[id]
	sort.Ints(childList)
	var children []*Node
	// Notice the _ which is the index and the child var
	// wich is the value of the mapping
	for _, child := range childList {
		children = append(children, appendToNode(child, parentToChildren))
	}

	return &Node{ID: id, Children: children}
}

// Build comment
func Build(records []Record) (*Node, error) {
	IDs := make(map[int]bool)
	var nodes []int
	parentToChildren := make(map[int][]int)

	fmt.Printf("")
	if len(records) == 0 {
		return nil, nil
	}

	for idx := range records {
		id := records[idx].ID
		parent := records[idx].Parent
		if IDs[id] {
			return nil, E.New("Duplicate ID")
		}

		if records[idx].ID == 0 && records[idx].Parent != 0 {
			return nil, E.New("root node has parent")
		}

		if id != 0 {
			parentToChildren[parent] = append(parentToChildren[parent], id)
		}

		if id == parent && id != 0 {
			return nil, E.New("cycle directly")
		}

		if parent > id {
			return nil, E.New("cycle indirectly")
		}
		nodes = append(nodes, id)
		IDs[id] = true
	}

	if !IDs[0] {
		return nil, E.New("no root node")
	}

	sort.Ints(nodes)
	for k, v := range nodes {
		if k != v {
			return nil, E.New("non-continuous")
		}
	}

	return appendToNode(0, parentToChildren), nil
}
