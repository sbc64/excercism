package flatten

import (
	"fmt"
	"reflect"
)

/*
// Flatten takes a nested list and return a single flattened list with all
// values except nil/null.
func Flatten(array interface{}) []interface{} {
	flattened := []interface{}{}
	for _, value := range array.([]interface{}) {
		switch v := value.(type) {
		case []interface{}:
			flattened = append(flattened, Flatten(v)...)
		case int:
			flattened = append(flattened, v)
		}
	}
	return flattened
}
*/

func extractArrays(input interface{}) []int {
	fmt.Printf("")
	temporarySlice := []int{}

	for idx := 0; idx < reflect.ValueOf(input).Len(); idx++ {
		typeOfAtIndex := reflect.TypeOf(reflect.ValueOf(input).Index(idx).Interface())
		if typeOfAtIndex == reflect.TypeOf(1) {
			value := reflect.ValueOf(input).Index(idx).Interface().(int)
			temporarySlice = append(temporarySlice, value)
		} else if typeOfAtIndex != reflect.TypeOf(nil) {
			value := reflect.ValueOf(input).Index(idx).Interface()
			temporarySlice = append(temporarySlice, extractArrays(value)...)
		}
	}

	return temporarySlice
}

// Flatten a
func Flatten(input interface{}) []interface{} {
	temporarySlice := extractArrays(input)
	var interfaceSlice []interface{} = make([]interface{}, len(temporarySlice))
	for i, val := range temporarySlice {
		interfaceSlice[i] = val
	}
	return interfaceSlice
}
