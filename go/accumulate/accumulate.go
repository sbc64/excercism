package accumulate

type operation func(string) string

// Accumulate hi
func Accumulate(given []string, operator operation) (output []string) {
	for _, value := range given {
		output = append(output, operator(value))
	}
	return
}
