package leap

// IsLeapYear should have a comment documenting it.
func IsLeapYear(input int) (isLeapYear bool) {

	if input%4 == 0 {
		isLeapYear = true
	}
	if input%100 == 0 {
		isLeapYear = false
	}
	if input%400 == 0 {
		isLeapYear = true
	}

	return isLeapYear
}
