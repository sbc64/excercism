package house

var mainSentence = []string{
	"This is the house that Jack built.",
	"This is the malt",
	"This is the rat",
	"This is the cat",
	"This is the dog",
	"This is the cow with the crumpled horn",
	"This is the maiden all forlorn",
	"This is the man all tattered and torn",
	"This is the priest all shaven and shorn",
	"This is the rooster that crowed in the morn",
	"This is the farmer sowing his corn",
	"This is the horse and the hound and the horn",
}

var notMainSentence = []string{
	"that lay in the house that Jack built.",
	"that ate the malt",
	"that killed the rat",
	"that worried the cat",
	"that tossed the dog",
	"that milked the cow with the crumpled horn",
	"that kissed the maiden all forlorn",
	"that married the man all tattered and torn",
	"that woke the priest all shaven and shorn",
	"that kept the rooster that crowed in the morn",
	"that belonged to the farmer sowing his corn",
}

// Verse a
func Verse(verseNumber int) (verse string) {
	verse = mainSentence[verseNumber-1]
	if verseNumber != 1 {
		verse += "\n"
	}
	for idx := verseNumber - 1; idx > 0; idx-- {
		verse += notMainSentence[idx-1]
		if idx != 1 {
			verse += "\n"
		}
	}
	return verse
}

// Song a
func Song() (song string) {
	for idx := 1; idx <= 12; idx++ {
		song += Verse(idx)
		if idx != 12 {
			song += "\n\n"
		}
	}
	return
}
