package hamming

import (
	E "errors"
)

//Distance calculates the Hamming distance between 2 DNA sequences
func Distance(a, b string) (int, error) {

	distance := 0

	if len(a) != len(b) {
		return 0, E.New("DNA sequence not the same amount")
	}

	for index, runeValue := range a {
		if runeValue != rune(b[index]) {
			distance++
		}
	}

	return distance, nil
}
