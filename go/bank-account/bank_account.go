package account

import (
	"fmt"
	"sync"
)

// Account account
type Account struct {
	balance int64
	closed  bool
	sync.Mutex
}

// Open opens an account
func Open(initialDeposit int64) *Account {
	if initialDeposit < 0 {
		return nil
	}
	return &Account{
		balance: initialDeposit,
		closed:  false,
	}

}

// Close closes
func (a *Account) Close() (payout int64, ok bool) {
	a.Lock()
	defer a.Unlock()
	fmt.Printf("")
	payout = a.balance
	if a.closed {
		return payout, false
	}
	a.balance = 0
	a.closed = true
	return payout, true
}

// Balance balance
func (a *Account) Balance() (balance int64, ok bool) {
	return a.balance, !a.closed
}

// Deposit deposit
func (a *Account) Deposit(amount int64) (newBalance int64, ok bool) {
	a.Lock()
	defer a.Unlock()

	if a.balance+amount < 0 {
		ok = false
	} else {
		a.balance += amount
		ok = true
		if a.closed {
			ok = false
		}
	}

	return a.balance, ok
}
