package raindrops

import (
	"strconv"
)

// Convert from number to Pling or Plang
func Convert(n int) (sound string) {

	if n%3 == 0 {
		sound += "Pling"
	}

	if n%5 == 0 {
		sound += "Plang"
	}

	if n%7 == 0 {
		sound += "Plong"
	}

	if sound == "" {
		return strconv.Itoa(n)
	}
	return
}
