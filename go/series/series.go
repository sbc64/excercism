package series

import (
	"fmt"
	"strings"
)

//All s
func All(window int, s string) []string {
	value := []string{}
	currentIndex := 0
	for {
		if window+currentIndex-1 == len(s) {
			break
		}
		currentSeries := []string{}
		// Sliding window
		for i := currentIndex; i < window+currentIndex; i++ {
			currentSeries = append(currentSeries, string(s[i]))
		}
		value = append(value, strings.Join(currentSeries, ""))
		currentIndex++
	}

	return value
}

//UnsafeFirst a
func UnsafeFirst(n int, s string) string {
	fmt.Println(string(s[0]))
	return s[0:n]
}
