package triangle

import "math"

type Kind string

const (
	NaT Kind = "NaT"
	Equ Kind = "Equ"
	Iso Kind = "Iso"
	Sca Kind = "Sca"
)

// KindFromSides should have a comment documenting it.
func KindFromSides(a, b, c float64) (k Kind) {
	// sort the triangles to make c the biggest number

	// NOT WOKRING
	var temp float64
	if a > c {
		temp = c
		c = a
		a = temp

	}
	if b > c {
		temp = c
		c = b
		b = temp
	}

	InfP := math.Inf(1)
	InfM := math.Inf(-1)

	if a == InfM || a == InfP || b == InfM || b == InfP || c == InfM || c == InfP {
		return NaT
	}

	if c <= b+a && a+b+c != 0 {
		if a-b == 0 && c-a == 0 {
			return Equ
		}

		if a == b || b == c || c == a {
			return Iso
		}

		return Sca

	}

	return NaT
}
