package isogram

import (
	S "strings"
	U "unicode"
)

// IsIsogram comment
func IsIsogram(word string) bool {
	word = S.ToLower(word)
	for i, runeV := range word {
		if S.IndexRune(word, runeV) != i && U.IsLetter(runeV) {
			return false
		}
	}
	return true
}
