with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "type-script-express";
  buildInputs = [ nodejs-12_x httpie ];
  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:$PATH"
  '';
}
