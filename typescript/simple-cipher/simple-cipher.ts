import crypto from "crypto"

const alphabet = "abcdefghijklmnopqrstuvwxyz".split("")
class SimpleCipher {
  key: string;
  constructor(key?: string) {
    this.key = key ? key : this.newKey() 
  }
  private newKey(): string {
    return crypto
      .randomBytes(100)
      .toString("base64")
      .toLowerCase()
      .replace(/[^a-z]/g, "");
  }
  encode(message: string): string {
    const cypherText: Array<string> = []
    message.split("").map((messageChar, idx) => {
      const charKey = this.key[idx%this.key.length]
      const shift =
        (alphabet.indexOf(charKey) + alphabet.indexOf(messageChar)) %
        alphabet.length;
      cypherText.push(alphabet[shift])
    })
    return cypherText.join("")
  }

  decode(cypher: string): string{
    const message: Array<string> = []
    cypher.split("").map((cypherChar, idx) => {
      const charKey = this.key[idx%this.key.length]
      let shift =
        (alphabet.indexOf(cypherChar) - alphabet.indexOf(charKey));
      if (shift < 0) {
        shift = 26+shift
      }
      message.push(alphabet[shift])
    })
    return message.join("")
  }
}

export default SimpleCipher;
