interface positions {
  black: Array<number>
  white: Array<number>, 
}
export default class QueenAttack {
  public white: Array<number>;
  public black: Array<number>;
  public board: string[] = new Array(8);
  constructor(positions: positions) {
    const { white, black } = positions;
    if (white[0] === black[0] && white[1] === black[1]) {
      throw new Error("Queens cannot share the same space");
    }
    this.white = white;
    this.black = black;
    for (let idx = 0; idx < this.board.length; idx++) {
      const tempRow = "________".split("");
      if (idx == white[0]) {
        tempRow[white[1]] = "W";
      }
      if (idx == black[0]) {
        tempRow[black[1]] = "B";
      }
      this.board[idx] = tempRow.join(" ");
    }
  }
  toString = (): string => this.board.join("\n") + "\n";
  diagonalAttack = (): boolean => {
    /*
       My first approach to solving this was trying
       to make a vector of the diagonals so that I could
       try to find if a point (the other piece) is in one of
       the vectors:

        0,1      
      "_ _ _ _ _ _ _ _",
      "_ _ _ _ _ _ _ _", 
      "_ _ _ _ _ _ _ _", 
      "_ _ _ _ _ _ _ _", 
      "_ _ _ _ _ _ _ _",  4,7
  5,6 "_ _ _ _ _ _ W _", 
      "_ _ _ _ _ _ _ _",  6,7
      "_ _ _ _ _ _ _ _ "  
              7,4

      This proved to be complicated because I had to do
      colinear calculations to the vector and the point.

      I later stumbled upon the idea of using the line equation:
      y=mx+b

      And realized that the slope of the chess board will always be
      -1 or 1. This also meant that when the x position of the white
      piece was not zero the b value would also change leading to
      one value of b0 corresponding to slope 1 and another value b1
      corresponding to slope -1

      So we start first by calculating the two lines using the white
      piece first (you can also use the black piece, it doesn't matter)

     line 1:
      y = 1 * x + (white.y - white.x)
     line 2:
      y = -1 * x + (white.y + white.x)

    We can then plug in the values of black in x and y and 
    if the equation is valid then the black piece is on the same
    line as the white piece

    The size of the board doesn't matter and with this solution
    I'm avoiding having to limit to ranges between 0 and 7
      */

    const positive =
      this.white[1] - this.white[0] + this.black[0] - this.black[1];
    const negative =
      this.white[1] + this.white[0] - this.black[0] - this.black[1];

    return negative == 0 || positive == 0;
  };
  canAttack = (): boolean =>
    this.white[0] === this.black[0] ||
    this.white[1] === this.black[1] ||
    this.diagonalAttack();
}

