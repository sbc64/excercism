pub fn is_leap_year(year: u64) -> bool {

    let mut is_leap_year = false;

    if year % 4 == 0 {
        is_leap_year = true;
    }
    if year % 100 == 0 {
        is_leap_year = false;
    }
    if year % 400 == 0 {
        is_leap_year = true;
    }
    return is_leap_year;
}
